<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class HomeController extends AbstractController
{
    /**
    * @Route("/home", name="home_route")
    */
    public function index(): Response
    {
        return $this->render(’home/index.html.twig’, [
        ’controller_name’ => ’HomeController’,
        ]);
    }

    /**
    * @Route("/menu", name="menu_route")
    */
    public function menu()
    {
    return $this->render(’shared/_menu.twig’, []);
    }
}

?>
