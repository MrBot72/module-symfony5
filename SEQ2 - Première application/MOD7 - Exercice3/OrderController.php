<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
* @Route("/order", name="order_")
*/
class OrderController
{
    /**
    * @Route("", name="index")
    */
    public function index()
    {
        return new Response("Liste des commandes");
    }
    /**
    * @Route("/{number}", name="show", requirements={
    * "number": "[A-Z0-9]+"
    * })
    *
    * Examples d'urls à saisir dans le navigateur :
    * /order/QUO123
    * /order/20190205CL
    */
    public function show(Request $request)
    {
        return new Response("Détail de la commande " . $request->attributes->get('number'));
    }
    /**
    * @Route("/{number}/product-add/{reference}/{quantity}", name="product_add", requirements={
    * "number": "[A-Z0-9]+",
    * "reference": "[A-Z-]+",
    * "quantity": "[0-9]+"
    * })
    *
    * Examples d'urls à saisir dans le navigateur :
    * /order/QUO123/product-add/APPLE-IPHONE/1
    * /order/20190205CL/product-add/SAMSUNG-GALAXY/2
    */
    public function productAdd(Request $request)
    {
        return new Response(
            "Ajout de " . $request->attributes->get('quantity') .
            " produit(s) " . $request->attributes->get('reference') .
            " à la commande " . $request->attributes->get('number')
        );
    }
}
