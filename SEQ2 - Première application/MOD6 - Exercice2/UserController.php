<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class UserController
{
    /** @Route("/user/create") */
    public function create()
    {
        return new Response("Nouvel utilisateur.");
    }
    /**
    * @Route("/user/{id}/show")
    *
    * Example d'url à saisir dans le navigateur :
    * /user/2/show
    */
    public function show($id)
    {
        return new Response("Détail de l'utilisateur $id.");
    }
    /** @Route(
    * "/user/{id}/update",
    * requirements={
    * "id": "[0-9]+"
    * }
    * )
    *
    * Example d'url à saisir dans le navigateur :
    * /user/2/update
    */
    public function update($id)
    {
        return new Response("Mise à jour de l'utilisateur $id.");
    }
    /** @Route(
    * "/user/{id}/set-state/{state}",
    * requirements={
    * "id": "[0-9]+",
    * "state": "[a-z]+"
    * }
    * )
    *
    * Example d'url à saisir dans le navigateur :
    * /user/5/set-state/valid
    */
    public function setState($id, $state)
    {
        return new Response("Mise à jour du statut de l'utilisateur $id à '$state'.");
    }
}
